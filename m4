import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * Reads and contains in memory the map of the game.
 *
 */
public class Map {

    private String mapNameRead;
    private String mapGold;

    /* Representation of the map */
    private char[][] map;

    /* Map name */
    private String mapName;

    /* Gold required for the human player to win */
    private int goldRequired;

    private Scanner x;
    String chosenMapFile;

    //char previousTail = '.';

    public int playerX, playerY, sizeX, sizeY;

    public int getPlayerX(){
        return playerX;
    }

    public int getPlayerY(){
        System.out.println(playerY);
        return playerY;
    }
    public char getTile(int[] coordinates) {
        if (map == null)
            System.out.println("Naspa man");
        System.out.println(map[coordinates[0]][coordinates[1]]);
        return map[coordinates[0]][coordinates[1]];
    }
    public void setTile(int[] coordinates, char a){
        //char tail = map[coordinates[0]][coordinates[1]];
        map[coordinates[0]][coordinates[1]] = a; //<-trebuie sa trimiti inapoi la harta///////////////////////////////////////////////////////////////////////
    }

    public void chooseMAP(String m) {
        try {
            //System.out.println(x);
            switch (m) {
                case "Very small Labyrinth of Doom":
                    chosenMapFile = "/home/mostwanted/Desktop/programing/DoD/src/map2.txt";
                    x = new Scanner(new File(chosenMapFile));
                    makeMap();
                    break;
                case "Small Labyrinth of Doom":
                    chosenMapFile = "/home/mostwanted/Desktop/programing/DoD/src/map1.txt";
                    x = new Scanner(new File(chosenMapFile));
                    makeMap();
                    break;
                default:
                    System.out.println("Choose one of the maps listed above.");
            }
        } catch (Exception e) {
            System.out.println("Could not find file.");
            System.out.println("Choose one of the maps listed above.");
        }
    }

    public String getMapName() {
        if (mapNameRead.startsWith("name")) {
            mapNameRead = mapNameRead.replace("name ", "");
            return mapNameRead;
        }
        else return null;
    }

    public int getGoldRequired() {
        //System.out.println(x);
        if (mapGold.startsWith("win")) {
            mapGold = mapGold.replace("win ", "");
            goldRequired = Integer.parseInt(mapGold);
            return goldRequired;
        }
        else return 0;
    }

    public int noOfIndex() {
        String line;
        int n = 3;
        try (Stream<String> stream = Files.lines(Paths.get(chosenMapFile))) {
            line = stream.skip(n).findFirst().get();
            //System.out.println(line);
            int a = line.length();
            return a;

        } catch (IOException e) {
            e.printStackTrace();
            return 20000;
        }
    }

    public int noOfLines() {
        try {
            long countOfLines = Files.lines(Paths.get(new File(chosenMapFile).getPath())).count(); //////////////////////////////////
            return (int) countOfLines - 2;
        } catch (IOException e) {
            System.out.println("No File Found");
        }
        return 0;
    }

    public char[][] getMap() {
        return map;
    }

    public void showMap() {
        //System.out.println(sizeX);
        //System.out.println(sizeY);
        for(int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++)
                System.out.print(map[i][j]);
            System.out.println();
        }
    }

    public void makeMap() {
        int i = 0;
        sizeX = noOfLines();
        sizeY= noOfIndex();

        if (x != null) {
            mapNameRead = x.nextLine();
            mapGold = x.nextLine();
            map = new char[sizeX][sizeY];
            while (x.hasNextLine()) {
                String a = x.nextLine();
                if (a.contains("#")) {
                    for (int j = 0; j < a.length(); j++) {
                        map[i][j] = a.charAt(j);
                        //System.out.print(map[i][j]);
                    }
                    i++;
                    //System.out.println();
                }
            }
        }
        placePlayer();
    }
    //int[] coordinates;

    public void closeFile() {
        x.close();
    }

    public void placePlayer() {
        int min = 1;
        Random randX = new Random();
        Random randY = new Random();
        int rand1 = randX.nextInt((sizeX - min) + 1) + min;
        int rand2 = randY.nextInt((sizeY - min) + 1) + min;
        //System.out.println("random x: " + rand1);
        //System.out.println("random y: " + rand2);
        while (true) {
            System.out.println("hello");
                if (map[rand1 - 1][rand2 - 1] == '.') {
                    map[rand1 - 1][rand2 - 1] = 'P';
                    playerX = rand1 - 1;
                    playerY = rand2 - 1;
                    break;
                }
                    else {
                    rand1 = randX.nextInt((sizeX - min) + 1) + min;
                    rand2 = randY.nextInt((sizeY - min) + 1) + min;
                }
        }
    }

}

    /**
     * Default constructor, creates the default map "Very small Labyrinth of doom".
     */
//    public Map() {//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        mapName = "Very small Labyrinth of Doom";
//        goldRequired = 2;
//        map = new char[][]{
//                {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'},
//                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#'},
//                {'#','.','.','.','.','.','.','G','.','.','.','.','.','.','.','.','.','E','.','#'},
//                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#'},
//                {'#','.','.','E','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#'},
//                {'#','.','.','.','.','.','.','.','.','.','.','.','G','.','.','.','.','.','.','#'},
//                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#'},
//                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#'},
//                {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'}
//        };
//    }

    /**
     * Constructor that accepts a map to read in from.
     *
     * @param : The filename of the map file.
     */
//    public Map(String fileName) {
//        readMap(fileName);////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    }

    /**
     * @return : Gold required to exit the current map.
     */
//    protected int getGoldRequired() {
//        return goldRequired;
//    }

    /**
     * @return : The map as stored in memory.
     */
//    protected char[][] getMap() {
//        return map;
//    }


    /**
     * @return : The name of the current map.
     */
//    protected String getMapName() {
//        return mapName;
//    }


    /**
     * Reads the map from file.
     *
     * @param : Name of the map's file.
     */
//    protected void readMap(String fileName) {//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    }

